﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfButtonsClicker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer dispatcherTimer;

        private string enter_string = "";
        private string output_string = "";
        private int startPoint = 0;
        private int index = 0;    

        private char[] doWorkArray;

        #region ButtonsData
        private char[] two = { 'a', 'b', 'c' };
        private char[] three = { 'd', 'e', 'f' };
        private char[] four = { 'g', 'h', 'i' };
        private char[] five = { 'j', 'k', 'l' };
        private char[] six = { 'm', 'n', 'o' };
        private char[] seven = { 'p', 'q', 'r', 's' };
        private char[] eight = { 't', 'u', 'v' };
        private char[] nine = { 'w', 'x', 'y', 'z' };
        #endregion

        private string EnterString
        {
            get { return enter_string; }
            set { enter_string = value; }
        }

        private string OutputString
        {
            get { return output_string; }
            set { output_string = value; }
        }

        private int Click
        {
            get { return index; }
            set { index = value; }
        }

        public string DoWork(char[] a, int click)
        {
            if (Click > a.Length)
            {
                startPoint = Click - a.Length - 1;
                Click = 0;
            }

            return EnterString += a[startPoint];            
        }

        public MainWindow()
        {
            InitializeComponent();
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 300);
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {            
            EnterTextBox.Text = DoWork(doWorkArray, Click);
            dispatcherTimer.Stop();
            OutputString += " ";
            startPoint = 0;
            Click = 0;
        }

        #region TypingButtons
        private void button_2_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();
            
            OutputString += "2";
            doWorkArray = two;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }

        private void button_3_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();

            OutputString += "3";
            doWorkArray = three;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }

        private void button_4_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();

            OutputString += "4";
            doWorkArray = four;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }

        private void button_5_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();

            OutputString += "5";
            doWorkArray = five;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }

        private void button_6_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();

            OutputString += "6";
            doWorkArray = six;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }

        private void button_7_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();

            OutputString += "7";
            doWorkArray = seven;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }

        private void button_8_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();

            OutputString += "8";
            doWorkArray = eight;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }

        private void button_9_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();

            OutputString += "9";
            doWorkArray = nine;

            startPoint = 0;
            startPoint += Click;
            Click++;

            dispatcherTimer.Start();
        }        

        private void button_0_Click(object sender, RoutedEventArgs e)
        {
            EnterString += " ";
            EnterTextBox.Text = EnterString;
            OutputString += "0";
        }
        
        private void button_1_Click(object sender, RoutedEventArgs e)
        {
            EnterString += ".";
            EnterTextBox.Text = EnterString;
            OutputString += "1";
        }
        #endregion

        private void Output_button_Click(object sender, RoutedEventArgs e)
        {
            Output_textBlock.Text = OutputString;
        }        
    }
}
