﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfButtonsClicker;

namespace UnitTestProject
{
    [TestClass]
    public class ButtonsClickerUnitTest
    {
        [TestMethod]
        public void DoWork_EnterString_returned()
        {
            //arrange
            int index = 0;
            char[] test_array = { 'a', 'b', 'c' };
            string testsEnterString = "a";

            //act
            MainWindow window = new MainWindow();
            string actual = window.DoWork(test_array, index);

            //assert
            Assert.AreEqual(testsEnterString, actual);
        }
    }
}
